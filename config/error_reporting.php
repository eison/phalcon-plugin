<?php
declare(ticks = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

use Eison\Phalcon\SystemError;

// +----------------------------------------------------------------------
// | Errors and exceptions
// +----------------------------------------------------------------------

/**
 * @package     config
 * @description Block all error if running on production environment
 * @author      eison.icu@gmail.com
 * @date        2021-06-25 16:03:40 via Ubuntu
 */
\error_reporting(E_ALL);

\set_error_handler(
    function (int $severity, string $message, string $fileName, int $lineNo) {
        $reporting = \error_reporting();

        if (0 === $reporting) {
            return;
        }

        if ($reporting & $severity) {
            throw new SystemError($message, 100001, $severity, $fileName, $lineNo);
        }
    }
);