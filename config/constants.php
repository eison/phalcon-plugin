<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | All Global constants are declare here.
// +----------------------------------------------------------------------

/**
 * Database action
 */
\define('DB_TRANS_START', 1);
\define('DB_TRANS_COMMIT', 2);
\define('DB_TRANS_RUNNING', 3);
\define('DB_TRANS_INTERRUPT', 4);

/**
 * Error code
 */
\define('ER_SUCCESS', 0);
\define('ER_UNLOGIN', 140011);
\define('ER_CREDENTIAL_EXPIRED', 140012);
\define('ER_INVALID_CREDENTIAL', 140013);
\define('ER_CALL_UNDFINED', 110001);
\define('ER_SQL_SEMANTIC', 110003);
\define('ER_NOT_PERMITTED', 110004);
\define('ER_INVALID_PARAMETER', 110006);
\define('ER_INPUT_VALIDATOR', 110007);
\define('ER_MODEL_VALIDATOR', 110008);
\define('ER_SOURCE_NOTFOUND', 110010);