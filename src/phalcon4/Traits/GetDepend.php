<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Traits;

use Phalcon\Di;
use Spatie\Macroable\Macroable;

/**
 * Trait GetDepend
 *
 * @package     Eison\Phalcon\Traits
 * @description Getting services more quickly in anywhere
 */
trait GetDepend
{
    /**
     * Register a custom macro.
     *
     * @class
     */
    use Macroable;

    /**
     * Calling method.
     *
     * @param string $method
     * @param mixed  $parameters
     * @return mixed
     */
    protected static function call(string $method, $parameters)
    {
        $macro = static::$macros[$method] ?? false;

        if ($macro instanceof \Closure) {
            return call_user_func(\Closure::bind($macro, null, static::class), $parameters);
        }

        return call_user_func($macro, $parameters);
    }

    /**
     * Import method/value/property into the current class table from id container.
     *
     * @param string $method
     * @param mixed  $parameters
     * @throws \Throwable
     * @throws \ReflectionException
     * @return mixed
     */
    protected static function __extract(string $method, $parameters): void
    {
        $service = lcfirst(preg_replace("/^get/i", '', $method));
        $container = Di::getDefault();

        if (!$container->has($service) && !static::hasMacro($method)) {
            exit("Service {$service} wasn't found in the dependency injection container.");
        }

        // When call a undefined method, dynamically add method to class.
        if ($container->has($service) && !static::hasMacro($method)) {
            $service = $container->get($service, $parameters);

            // Dynamically add methods to a class.
            static::macro($method, function () use (&$service) {
                return $service;
            });
        }
    }

    /**
     * Utilized for reading data from non-existing properties.
     *
     * @param string $property
     * @throws \Throwable
     * @throws \ReflectionException
     * @return mixed
     */
    final public function __get(string $property)
    {
        // Default return service from di container.
        $consequence = Di::getDefault()->getShared($property);

        if (!$consequence) {
            exit("Service {$property} wasn't found in the dependency injection container.");
        }

        // When call a undefined property, dynamically add property to class.
        return $this->{$property} = $consequence;
    }

    /**
     * Utilized for reading data from non-existing method.
     *
     * @param string $method
     * @param mixed  $parameters
     * @throws \Throwable
     * @throws \ReflectionException
     * @return mixed
     */
    final public function __call(string $method, $parameters)
    {
        // Import method into the class table.
        self::__extract($method, $parameters);

        // Calling method.
        return self::call($method, $parameters);
    }

    /**
     * Utilized for reading data from non-existing method.
     *
     * @param string $method
     * @param mixed  $parameters
     * @throws \Throwable
     * @throws \ReflectionException
     * @return mixed
     */
    final public static function __callStatic(string $method, $parameters)
    {
        // Import method into the class table.
        self::__extract($method, $parameters);

        // Calling method.
        return self::call($method, $parameters);
    }
}