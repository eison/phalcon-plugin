<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Traits;

use Eison\Phalcon\PhalconPluginError;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Text;

/**
 * Trait Cache
 *
 * @package     Eison\Phalcon\Traits
 * @description If you want to force cache result, import this trait into the model class.
 */
trait Cache
{
    /**
     * The status of cache model.
     *
     * @var bool
     */
    protected static $enableCache = true;

    /**
     * A string that will be prepended to every key created.
     *
     * @var string
     */
    protected static $masterKey = 'models';

    /**
     * Your cache service name.
     *
     * @var string
     */
    protected static $cacheServiceName = 'modelsCache';

    /**
     * Dnables/Disables cache model.
     *
     * @param bool $toggle
     * @return void
     */
    public static function enableCache(bool $toggle = null): void
    {
        if (is_bool($toggle)) {
            self::$enableCache = $toggle;
        }
    }

    /**
     * Sets the master key.
     *
     * @param string $key
     * @return void
     */
    public static function setMasterKey($key = null): void
    {
        self::$masterKey = $key;
    }

    /**
     * Sets Phalcon Cache service name.
     *
     * @param string $serviceName
     * @return void
     */
    public static function setCacheServiceName($serviceName = null): void
    {
        self::$cacheServiceName = $serviceName;
    }

    /**
     * Returns unique cache key for provided paramaters and key.
     *
     * @param mixed  $parameters
     * @param string $key
     * @return string
     */
    protected static function createKey($parameters, string $key): string
    {
        // Default unset di containers
        unset($parameters['di']);

        $modelName = get_called_class();
        $cacheKey = md5(serialize($parameters));

        // Replace namespace slashes if used
        return sprintf('%s:%s:%s:%s', self::$masterKey, $modelName, $key, $cacheKey);
    }

    /**
     * Creates unique cache key and inserts it into the paramaters.
     *
     * @param mixed  $parameters
     * @param string $key
     * @return array
     */
    protected static function makeCacheOptions($parameters, string $key): array
    {
        if (!self::$enableCache) {
            return $parameters;
        }

        $parameters = (array)$parameters;

        if (!isset($parameters['cache'])) {
            $key = self::createKey($parameters, $key);
            $parameters['cache'] = compact('key');
        }

        return $parameters;
    }

    /**
     * Public alias for self::createKey to be used in raw sql queries.
     *
     * @param mixed  $parameters
     * @param string $key
     * @return string
     */
    public static function getCacheKey($parameters, string $key): string
    {
        return self::createKey($parameters, $key);
    }

    /**
     * Model::find doesn't create any model instances, doesn't have to return full model, because of performance.
     * You can set custom resultset class for models.
     *
     * @param null $parameters
     * @return ResultsetInterface
     * resultset only store plain array result from database, even without columns mapping etc.
     */
    public static function find($parameters = null): ResultsetInterface
    {
        $parameters = self::makeCacheOptions($parameters, 'find');

        return parent::find($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function findFirst($parameters = null): ?ModelInterface
    {
        $parameters = self::makeCacheOptions($parameters, 'findFirst');

        return parent::findFirst($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function count($parameters = null)
    {
        $parameters = self::makeCacheOptions($parameters, 'count');

        return parent::count($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function sum($parameters = null)
    {
        $parameters = self::makeCacheOptions($parameters, 'sum');

        return parent::sum($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function maximum($parameters = null)
    {
        $parameters = self::makeCacheOptions($parameters, 'maximum');

        return parent::maximum($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function minimum($parameters = null)
    {
        $parameters = self::makeCacheOptions($parameters, 'minimum');

        return parent::minimum($parameters);
    }

    /**
     * Creates unique cache key for provided paramaters and checks if availabe.
     *
     * @param mixed $parameters
     * @return mixed
     */
    public static function average($parameters = null)
    {
        $parameters = self::makeCacheOptions($parameters, 'average');

        return parent::average($parameters);
    }

    /**
     * Overrides parent method to cache when getter is used ($robot->getRobotsParts()).
     * Returns related records defined relations depending on the method name
     *
     * @param string modelName
     * @param string method
     * @param array  arguments
     * @return mixed
     */
    protected function _getRelatedRecords($modelName, $method, $arguments)
    {
        $manager = $this->_modelsManager;
        $relation = false;
        $queryMethod = null;

        // Calling find/findFirst if the method starts with "get"
        if (Text::startsWith($method, 'get')) {
            $alias = substr($method, 3);
            $relation = $manager->getRelationByAlias($modelName, $alias);
        }

        // Calling count if the method starts with "count"
        else if (Text::startsWith($method, 'count')) {
            $alias = substr($method, 5);
            $queryMethod = 'count';
            $relation = $manager->getRelationByAlias($modelName, $alias);
        }

        // If the relation was found perform the query via the models manager
        if (!is_object($relation)) {
            return null;
        }

        $extraArgs = $arguments[0] ?? null;

        // Check if cache is enabled and the relation type is many to many
        // If its belongs to or has many it will invoke findFirst/find so no need for caching it twice
        if (self::$enableCache && $relation->getType() > 2 && $this->di->has(self::$cacheServiceName)) {
            $arguments = (array)$arguments;
            $extraParameters = (array)$relation->getParams();

            // Making sure that if the conditions are the same the key would match everytime using getters or properties
            if (empty($arguments)) {
                $arguments = [null];
            }

            $extraParameters['method'] = $queryMethod ?: 'get';
            $keyCaption = array_merge($arguments, $extraParameters);

            // Getting fields used in relationship
            $fields = $relation->getFields();

            if (!is_array($fields)) {
                $fields = [$fields];
            }

            // Making a unique key based on field's value.
            foreach ($fields as $key => $value) {
                $keyCaption['APR' . $key] = $this->readAttribute($value);
            }

            // Create key
            $cacheKey = self::createKey($keyCaption, strtolower($alias));
            $cacheService = $this->di->get(self::$cacheServiceName);

            if ($cacheService->exists($cacheKey)) {
                return $cacheService->get($cacheKey);
            } else {
                $result = $manager->getRelationRecords($relation, $queryMethod, $this, $extraArgs);
                $cacheService->save($cacheKey, $result);

                return $result;
            }
        }

        return $manager->getRelationRecords($relation, $queryMethod, $this, $extraArgs);
    }


    /**
     * Overrides parent method to cache when getter is used ($robot->getRelated('robotsParts')).
     * Returns related records based on defined relations
     *
     * @param string alias
     * @param array  arguments
     * @throws \Exception
     * @return mixed
     */
    public function getRelated($alias, $arguments = null)
    {
        // Query the relation by alias
        $className = get_class($this);
        $manager = $this->_modelsManager;
        $relation = $manager->getRelationByAlias($className, $alias);

        if (!is_object($relation)) {
            throw new PhalconPluginError(
                sprintf("Undefined relations for the model '%s' using alias '%s'", $className, $alias)
            );
        }

        // Check if cache is enabled and the relation type is many to many
        // If its belongs to or has many it will invoke findFirst/find so no need for caching it twice
        if (self::$enableCache && $relation->getType() > 2 && $this->di->has(self::$cacheServiceName)) {
            if (!is_array($arguments)) {
                $arguments = [$arguments];
            }

            $extraParameters = $relation->getParams();

            if (!is_array($extraParameters)) {
                $extraParameters = [$extraParameters];
            }

            // Making sure that if the conditions are the same
            // The key would match everytime using getters or properties
            $extraParameters['method'] = 'get';

            $keyCaption = array_merge($arguments, $extraParameters);

            // Getting fields used in relationship
            $fields = $relation->getFields();

            if (!is_array($fields)) {
                $fields = [$fields];
            }

            // Making a unique key based on field's value
            foreach ($fields as $key => $value) {
                $keyCaption['APR' . $key] = $this->readAttribute($value);
            }

            // Create key
            $cacheKey = self::createKey($keyCaption, strtolower($alias));
            $cacheService = $this->di->get(self::$cacheServiceName);

            if ($cacheService->exists($cacheKey)) {
                return $cacheService->get($cacheKey);
            } else {
                $result = $manager->getRelationRecords($relation, null, $this, $arguments);
                $cacheService->save($cacheKey, $result);

                return $result;
            }
        }

        // Call the 'getRelationRecords' in the models manager
        return $manager->getRelationRecords($relation, null, $this, $arguments);
    }

    /**
     * Deletes all cache for model with the option to delete other related models
     *
     * @param bool              $resetRelated delete all related models
     * @param null|string|array $otherModels  other optional models to delete
     * @return void
     */
    public function resetCache($resetRelated = false, $otherModels = null): void
    {
        if (!self::$enableCache) {
            return;
        }

        $modelName = get_called_class();
        $masterKey = self::$masterKey;
        $models = [$modelName];

        // Check for provided other models to remove cache for
        if (!is_null($otherModels)) {
            if (!is_array($otherModels)) {
                $otherModels = [$otherModels];
            }

            $models = array_merge($models, $otherModels);
        }

        // Get all related models when its set to true
        if ($resetRelated === true) {
            $manager = $this->_modelsManager;

            $relations = array_merge(
                $manager->getHasMany($this),
                $manager->getHasOne($this),
                $manager->getHasManyToMany($this),
                $manager->getHasOneAndHasMany($this)
            );

            foreach ($relations as $relation) {
                $models[] = $relation->getReferencedModel();
                $intermediateModel = $relation->getIntermediateModel();

                if ($intermediateModel) {
                    $models[] = $intermediateModel;
                }
            }
        }

        // Remove duplicate models
        $models = array_unique($models);

        // Replace namespace slashes if used
        $models = array_map(
            function ($model) {
                return str_replace('\\', '_', $model);
            },
            $models
        );

        $modelsCache = $this->getDi()->get(self::$cacheServiceName);
        $keys = $modelsCache->queryKeys($masterKey);

        foreach ($keys as $key) {
            if ($resetRelated === 'ALLCACHE') {
                $modelsCache->delete($key);
                continue;
            }

            foreach ($models as $model) {
                if (strpos($key, $model) !== false) {
                    $modelsCache->delete($key);
                    break;
                }
            }
        }
    }
}