<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Traits;

use Eison\Phalcon\PhalconPluginError;
use Eison\Phalcon\Library\Collection;
use Phalcon\Helper\Arr;

/**
 * Trait Behaviors
 *
 * @package     Eison\Phalcon\Traits
 * @description Events can be used to assign values before performing an operation.
 * @see https://docs.phalcon.io/4.0/en/db-models-events
 */
trait Behaviors
{
    /**
     * Scope: Insert/Update
     * Can not stop operation.
     * Is executed before saving and allows data manipulation.
     *
     * @param int $begin nesting
     * @return void
     */
    protected function prepareSave(int $begin = \DB_TRANS_START): void
    {
        // Trigger an aop Hook
        $this->fireEvent('prepareSaveHook');

        // Start a transaction, if is set.
        // Prevents transaction nesting.
        if ($this->autoTrans() === \DB_TRANS_START && !
            $this->getStore()->get('db_transactions')->has('begin')
        ) {
            $this->getDb()->begin(true);
            $this->getStore()->append('db_transactions', compact('begin'));
        }
    }

    /**
     * Invalid parameter | Model validate
     *
     * Scope: Insert/Update
     * Can stop operation.
     * Is executed after an integrity validator fails.
     *
     * @throws \Throwable
     * @throws \ReflectionException
     * @return void
     */
    protected function onValidationFails(): void
    {
        // Trigger an aop Hook.
        $this->fireEvent('onValidationFailsHook');

        // The model failed to save, so rollback the transaction.
        if ($this->autoTrans() && $this->getStore()->get('db_transactions')->has('begin')) {
            $this->getDb()->rollback();
        }

        $messages = $this->getMessages();
        $message = sprintf('%s:%s', static::class, Arr::last($messages)->getMessage());

        throw new PhalconPluginError($message, PhalconPluginError::ER_MODEL_VALIDATOR);
    }

    /**
     * Can not stop operation.
     * Runs when records are not saved (fail).
     *
     * @throws \Throwable
     * @throws \ReflectionException
     * @return void
     */
    protected function notSaved(): void
    {
        // Trigger an aop Hook.
        $this->fireEvent('notSavedHook');
        
        // The model failed to save, so rollback the transaction.
        if ($this->autoTrans() && $this->getStore()->get('db_transactions')->has('begin')) {
            $this->getDb()->rollback();
        }

        $messages = $this->getMessages();
        $message = sprintf('%s:%s', static::class, Arr::last($messages)->getMessage());

        throw new PhalconPluginError($message, PhalconPluginError::ER_MODEL_NOT_SAVED);
    }

    /**
     * Scope: Insert/Update
     * Can not stop operation.
     * Runs after saving a record.
     * 
     * @return void
     */
    protected function afterSave(): void
    {
        // Trigger an aop Hook
        $this->fireEvent('afterSaveHook');

        // Save the result of earch successful step in the work follw into the consequences.
        // This consequences can be used to check the work follw.
        $this->getStore()->append('wf_consequences', [static::class => $this]);

        // Commit the transaction if start transaction.
        if (($this->autoTrans() === \DB_TRANS_COMMIT && !$this->getStore()->get('db_transactions')->get('interrupt')) ||
            ($this->getStore()->get('db_transactions')->has('dbcommit'))
        ) {
            $this->getDb()->commit();
            $this->getStore()->set('db_transactions', new Collection());
        }
    }

    /**
     * Can not stop operation.
     * The afterFetch is fired when model instance is created,
     * or after selecting certain record from resultset,don't get fired on find and toArray in any case.
     * 
     * @return void
     */
    protected function afterFetch(): void
    {
        // Trigger an aop Hook.
        $this->fireEvent('afterFetchHook');
    }
}