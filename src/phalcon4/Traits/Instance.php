<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Traits;

/**
 * Trait Instance
 *
 * @package     Eison\Phalcon\Traits
 * @description Create an static instance
 */
trait Instance
{
    /**
     * Class.
     *
     * @var object
     */
    protected static $intance;

    /**
     * Create a new instance of this class.
     *
     * @param array | stdClass $parameters
     * @return static
     */
    public static function newInstance($parameters = []): object
    {
        if (!static::$intance instanceof static) {
            static::$intance = new static($parameters);
        }

        return self::$intance;
    }
}