<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Events;

use Eison\Phalcon\Injectable;
use Exception;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

/**
 * Class NotfoundListener
 *
 * @package     Eison\Phalcon\Events
 * @description Before the dispatcher throws any exception
 */
class NotfoundListener extends Injectable
{
    /**
     * Hook
     *
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @param Exception  $exception
     * @return bool
     */
    public function beforeException(Event $event, Dispatcher $dispatcher, Exception $exception)
    {
        // Controller class not found
        if ($exception instanceof \ReflectionException) {
            static::logger()->error($exception->getMessage());
            static::response()->setStatusCode(404)->setContent('Route Not Found :(')->send();

            // Interrupt and return;
            exit;
        }

        return false;
    }
}