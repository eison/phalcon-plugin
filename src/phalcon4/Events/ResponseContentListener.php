<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Events;

use Eison\Phalcon\Injectable;
use Eison\Phalcon\SystemError;
use Eison\Utils\PhpClass\PhpDatum;
use Phalcon\Events\Event;
use Phalcon\Helper\Json as JsonHelper;
use Phalcon\Mvc\Dispatcher as Dispatcher;

/**
 * Class ResponseContentListener
 *
 * @package     Eison\Phalcon\Events
 * @description After executing the controller/action method.
 */
class ResponseContentListener extends Injectable
{
    /**
     * Content body paramers
     *
     * @var string
     */
    protected $message = 'ok', $status = 'success';

    /**
     * Gets message.
     *
     * @return string
     */
    protected function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Gets status.
     *
     * @return string
     */
    protected function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Sets HTTP response body. The parameter is automatically converted to json,
     * so you must sets default header: Content-Type: "application/json; charset=UTF-8" before.
     *
     * @param mixed $consequence
     * @return array
     */
    protected function make($consequence): array
    {
        $page = $this->dispatcher->pageCount;
        
        return [
            'code'    => $consequence ? SystemError::ER_SUCCESS : SystemError::ER_RESOURCE_NOTFOUND,
            'status'  => $this->getStatus(),
            'message' => $this->getMessage(),
            'data'    => $consequence,
        ];
    }

    /**
     * Hook
     *
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function afterDispatch(Event $event, Dispatcher $dispatcher)
    {
        $collection = $dispatcher->getReturnedValue() ?: $dispatcher->getLastController()->getReturnedValue();
        $collection = PhpDatum::isObject($collection) ? $collection->toArray() : $collection;

        echo JsonHelper::encode($this->make($collection));
    }
}