<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

use Phalcon\Di;
use Phalcon\Helper\Json;
use Phalcon\Logger;

/**
 * Class PhalconPluginError
 *
 * @package     Eison\Phalcon
 * @description App interface
 * @author      eison.icu@gmail.com
 * @date        2021-06-17 17:34:09 via Ubuntu
 */
class PhalconPluginError extends SystemError
{
    /**
     * Logs with an arbitrary level.
     *
     * @param int    $level
     * @param string $message
     * @param array  $context
     * @return \Throwable
     */
    protected function record(int $level = Logger::DEBUG, string $message = null, array $context = []): \Throwable
    {
        $message = $message ?: $this->getMessage();

        // Record the original exception message
        Di::getDefault()->getLogger()->log($level, $message, $context);

        return $this;
    }

    /**
     * Listen the exceptions.
     *
     * @return void
     */
    public function listen(): void
    {
        // Reports information about a class.
        $reflectionClass = new \ReflectionClass($this);

        // Record the original error and parse the error message.
        $this->record(Logger::ERROR);
        $this->setStatus('fail');
        $this->setPayload($reflectionClass->getShortName(), 'exception');

        // Reporting error
        echo Json::encode($this->getPayloads());
    }
}