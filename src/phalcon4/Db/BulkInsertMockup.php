<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Db;

use Eison\Phalcon\Abstracts\AbstractMockup;
use Eison\Phalcon\Interfaces\MockupInterface;
use Eison\Phalcon\Traits\Instance;
use Eison\Utils\Helper\ArrHelper;
use Phalcon\Messages\Message;
use Phalcon\Mvc\ModelInterface;

/**
 * Class BulkInsertMockup
 *
 * @package     Eison\Phalcon\Db
 * @description This component is use a lower level database manipulation，to parse and allows batch inserts.
 */
class BulkInsertMockup extends AbstractMockup
{
    /**
     * Create an static instance.
     *
     * @class
     */
    use Instance;

    private $bindRows;
    private $rowsString;
    private $result = false;
    private $bindString = '';

    protected $model;

    /**
     * @inheritdoc
     */
    public function validate(): void
    {
        $columnCount = count($this->bindRows);

        // Fires an event, implicitly calls behaviors.
        if ($columnCount === 0) {
            $message = new Message('Bindrows cannot be empty');
            $this->getModel()->appendMessage($message)->fireEvent('onValidationFails');
        }
    }

    /**
     * @inheritdoc
     */
    public function queryBuilder($ignore): string
    {
        // Validates the data before calling sql.
        $this->validate();

        if ($ignore) {
            $rawSql = "INSERT IGNORE INTO `%s` (%s) VALUES %s";
        } else {
            $rawSql = "INSERT INTO `%s` (%s) VALUES %s";
        }

        return sprintf($rawSql, $this->getModel()->getSource(), $this->rowsString, $this->bindString);
    }

    /**
     * BulkInsertMockup constructor.
     *
     * @param ModelInterface $model
     * @return void
     */
    public function __construct(ModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * Set the rows.
     * Metadata must be defined with the current mockup.
     *
     * @param array|string $rows
     * @return MockupInterface
     */
    public function setRows($rows): MockupInterface
    {
        $this->bindRows = $this->getFilter($rows);
        $this->rowsString = $this->makeRowsString($this->bindRows);

        return $this;
    }

    /**
     * Set the values.
     *
     * @param mixed $bindValues
     * @return MockupInterface
     */
    public function setValues($bindValues): MockupInterface
    {
        foreach (scrimp($bindValues) as $datums) {
            //--- Performance Optimizations. ---//
            if (count($datums) != count($this->bindColumns)) {
                $datums = ArrHelper::getValue($this->bindColumns, $datums);
            } else {
                ksort($datums);
            }

            $tmpStr = implode("','", $datums);

            $this->bindString .= '(\'' . $tmpStr . '\'),';
        }

        $this->bindString = rtrim($this->bindString, ',');

        return $this;
    }

    /**
     * Insert into the database.
     *
     * @param bool $ignore use an insert ignore (default: false)
     * @return ModelInterface
     */
    public function insert(bool $ignore = false): ModelInterface
    {
        // Fires an event, implicitly calls behaviors.
        $this->getModel()->fireEvent('beforeValidationOnCreate');
        $this->getModel()->fireEvent('prepareSave');

        $rawSql  = $this->queryBuilder($ignore);
        $schema  = $this->getModel()->getSchema();
        $connect = $this->getModel()->getReadConnection();

        // Execute sql.
        $connect->execute(sprintf('use %s;', $schema));
        $mockupResult = $connect->query($rawSql);

        $this->getModel()->mockupResult = $mockupResult;

        return $this->getModel();
    }
}