<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Db;

use Eison\Phalcon\Interfaces\MockupInterface;
use Eison\Utils\Helper\ArrHelper;
use Phalcon\Mvc\ModelInterface;

/**
 * Class DuplicateMockup
 *
 * @package     App\Library
 * @description INSERT INTO ... ON DUPLICATE KEY UPDATE ...
 */
class DuplicateMockup extends BulkInsertMockup
{
    /**
     * Gets update conditions.
     *
     * @return string
     */
    protected function getUpdateCond(): string
    {
        if (isset($this->updateCond)) {
            return $this->updateCond;
        }

        // Returns the name of primary key.
        $pk = $this->getModel()->getModelsMetaData()->getIdentityField($this->getModel());

        return sprintf('`%s`=values(`%s`)', $pk, $pk);
    }

    /**
     * Build rawSql
     *
     * @return string
     */
    protected function buildRawSql(): string
    {
        return sprintf('%s ON DUPLICATE KEY UPDATE %s', $this->queryBuilder(false), $this->getUpdateCond());
    }

    /**
     * On duplicate key update (columns).
     *
     * @param array|string $columns
     * @param string       $character
     * @return string
     */
    protected function blend($columns, string $character = ''): string
    {
        $columns = ArrHelper::getValue($columns, $this->getColumMap());

        foreach ((array)$columns as $column) {
            if ($column) {
                $character .= sprintf('`%s`=values(`%s`),', $column, $column);
            }
        }

        return rtrim($character, ',');
    }

    /**
     * @inheritdoc
     */
    public function getRawSql($ignore = false): string
    {
        return $this->buildRawSql();
    }

    /**
     * Sets update conditionals.
     *
     * @param string|array $update
     * @return MockupInterface
     */
    public function setUpdate($update): MockupInterface
    {
        $this->updateCond = $this->blend($update);

        return $this;
    }

    /**
     * INSERT INTO ... ON DUPLICATE KEY UPDATE ...
     *
     * @return \Phalcon\Mvc\ModelInterface
     */
    public function duplicate(): ModelInterface
    {
        // Fires an event, implicitly calls behaviors.
        $this->getModel()->fireEvent('beforeValidationOnCreate');
        $this->getModel()->fireEvent('prepareSave');

        $rawSql  = $this->buildRawSql();
        $schema  = $this->getModel()->getSchema();
        $connect = $this->getModel()->getReadConnection();

        // Execute sql.
        $connect->execute(sprintf('use %s;', $schema));
        $mockupResult = $connect->query($rawSql);

        $this->getModel()->mockupResult = $mockupResult;

        return $this->getModel();
    }
}