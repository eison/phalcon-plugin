<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Db;

use Eison\Phalcon\Abstracts\AbstractMockup;
use Eison\Phalcon\Interfaces\MockupInterface;
use Eison\Phalcon\Traits\Instance;
use Phalcon\Messages\Message;
use Phalcon\Mvc\ModelInterface;

/**
 * Class BatchUpdateMockup
 *
 * @package     Eison\Phalcon\Db
 * @description This component is use a lower level database manipulation，to parse and allow batch inserts.
 */
class BatchUpdateMockup extends AbstractMockup
{
    /**
     * Create an static instance.
     *
     * @class
     */
    use Instance;

    /**
     * Store the execute result.
     *
     * @var bool
     */
    private $result = false;
    private $placeholders = [];

    /**
     * @todo [Tutorial] The expression and the field must be separated by a space. is aaaaaaaaa.
     *
     * @param mixed  $value String or array
     * @param array  $columMap
     * @param string $column
     * @return string
     */
    private function readOperate(string $column, &$value, array &$columMap): string
    {
        if (strpos($column,' ') === false) {
            exit("Column '{$column}' does not belong to any object");
        }

        list($column, $expression) = explode(' ', $column);

        $expression = strtoupper(trim($expression));

        if ('IN' == $expression) {
            $value = is_string($value) ? explode(',', $value) : $value;
            $lenght = count($value);
            $placeholder = rtrim(str_repeat('?,', $lenght), ',');

            return sprintf('`%s` IN (%s)', $columMap[$column], $placeholder);
        }

        elseif ('NOTIN' == $expression) {
            $value = is_string($value) ? explode(',', $value) : $value;
            $lenght = count($value);
            $placeholder = rtrim(str_repeat('?,', $lenght), ',');

            return sprintf('`%s` NOT IN (%s)', $columMap[$column], $placeholder);
        }

        elseif ('>=' == $expression) {
            return sprintf('`%s` >= ?', $columMap[$column]);
        }

        elseif ('<=' == $expression) {
            return sprintf('`%s` <= ?', $columMap[$column]);
        }

        exit("Operators not yet resolved: '{$expression}'");
    }

    /**
     * Metadata must be defined with the current mockup.
     *
     * @param array  $paramters
     * @param string $separator
     * @param array  $collection
     * @return string
     */
    private function parseString(array $paramters, string $separator, array $collection = []): string
    {
        $columMap = $this->getColumMap();

        foreach ($paramters as $column => $value) {
            if (!is_null($value)) {
                // If an expression exists in the field,
                if (!isset($columMap[$column])) {
                    $course = $this->readOperate($column, $value, $columMap);
                } else {
                    $course = sprintf('`%s` = ?', $columMap[$column]);
                }

                array_push($collection, $course);
                array_push($this->placeholders, ...(array)$value);
            }
        }

        return trim(implode($separator, $collection));
    }

    /**
     * @inheritdoc
     */
    public function validate(string $message = null): void
    {
        if (!isset($this->bindPayload)) {
            $message = new Message('BindPayload cannot be empty');
        }

        if (!isset($this->bindCondition)) {
            $message = new Message('BindCondition cannot be empty');
        }

        if (!is_null($message)) {
            // Fires an event, implicitly calls behaviors.
            $this->getModel()->appendMessage($message)->fireEvent('onValidationFails');
        }
    }

    /**
     * @inheritdoc
     */
    public function queryBuilder($ignore = false): string
    {
        // Validates the data before calling sql.
        $this->validate();

        $rawSql = 'UPDATE `%s` SET %s WHERE %s';

        return \sprintf($rawSql, $this->getModel()->getSource(), $this->bindPayload, $this->bindCondition);
    }

    /**
     * BatchUpdateMockup constructor.
     *
     * @param ModelInterface $model
     * @return void
     */
    public function __construct(ModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * Sets payloads.
     *
     * @param array|string $payload
     * @return MockupInterface
     */
    public function setPayload($payload): MockupInterface
    {
        if (gettype($payload) === 'array') {
            $payload = $this->parseString($payload, ',');
        }

        $this->bindPayload = $payload;

        return $this;
    }

    /**
     * Sets conditionals.
     *
     * @param $conditional
     * @return MockupInterface
     */
    public function setCondition($conditional): MockupInterface
    {
        if (gettype($conditional) === 'array') {
            $conditional = $this->parseString($conditional, ' AND ');
        }

        $this->bindCondition = $conditional;

        return $this;
    }

    /**
     * Update records.
     *
     * @throws AppError
     * @return ModelInterface
     */
    public function update()
    {
        // Fires an event, implicitly calls behaviors.
        $this->getModel()->fireEvent('prepareSave');

        $rawSql  = $this->queryBuilder();
        $schema  = $this->getModel()->getSchema();
        $connect = $this->getModel()->getReadConnection();

        // Execute sql.
        $connect->execute(sprintf('use %s;', $schema));
        $mockupResult = $connect->query($rawSql, $this->placeholders);

        $this->getModel()->mockupResult = $mockupResult;

        return $this->getModel();
    }
}