<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Db;

use Eison\Phalcon\Abstracts\Model;
use Eison\Phalcon\Traits\GetDepend;
use Phalcon\Filter;
use Phalcon\Helper\Str;
use Phalcon\Mvc\Model\Criteria as ModelCriteria;
use Phalcon\Mvc\Model\CriteriaInterface as CriteriaInterface;
use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * Class Criteria
 *
 * @description This class is used to build the find?(First) using an object-oriented interface.
 * @package     Eison\Phalcon\Abstracts\Model
 */
class Criteria extends ModelCriteria
{
    /**
     * Getting services more quickly in anywhere.
     *
     * @class
     */
    use GetDepend;

    /**
     * Model alias.
     *
     * @var
     */
    protected $alias;

    /**
     * The total number of paginated items.
     *
     * @var
     */
    protected $pgCount;

    /**
     * Sets the hydration mode in the resultset, works on model created!!
     *
     * @param int $hydrateMode
     * @return ResultsetInterface
     */
    protected function setHydrateMode(int $hydrateMode): ResultsetInterface
    {
    }

    /**
     * @inheritdoc
     */
    public function getParams(): array
    {
        $params = parent::getParams();

        if ($this->alias) {
            if (isset($params['columns'])) {
                $params['columns'] = preg_replace("/{$this->alias}\./i", $this->getModelName() . '.', $params['columns']);
            }
        }

        return $params;
    }

    /**
     * @inheritdoc
     */
    public function columns($columns): CriteriaInterface
    {
        if (is_string($columns) && Str::startsWith($columns, 'excl:')) {
            $columnMaps = $this->modelsMetadata->getReverseColumnMap((new $this->model()));

            $columns = preg_replace('/^excl:/i', '', $columns);
            $columns = array_diff(array_keys($columnMaps), explode(',', $columns));
        }

        return parent::columns($columns);
    }

    /**
     * @inheritdoc
     */
    public function join(string $model, $conditions = null, $alias = null, $type = null): CriteriaInterface
    {
        if (is_string($conditions) &&
            $this->alias
        ) {
            $conditions = preg_replace("/{$this->alias}\./i", $this->getModelName() . '.', $conditions);
        }

        return parent::join($model, $conditions, $alias, $type);
    }

    /**
     * @inheritdoc
     */
    public function leftJoin(string $model, $conditions = null, $alias = null): CriteriaInterface
    {
        if (is_string($conditions) &&
            $this->alias
        ) {
            $conditions = preg_replace("/{$this->alias}\./i", $this->getModelName() . '.', $conditions);
        }

        return parent::leftJoin($model, $conditions, $alias);
    }

    /**
     * @inheritdoc
     */
    public function groupBy($group): CriteriaInterface
    {
        if ($this->alias) {
            $group = preg_replace("/{$this->alias}\./i", $this->getModelName() . '.', $group);
        }

        return parent::groupBy($group);
    }

    /**
     * @inheritdoc
     */
    public function andWhere(string $conditions, $bindParams = null, $bindTypes = null): CriteriaInterface
    {
        if ($this->alias) {
            $conditions = preg_replace("/{$this->alias}\./i", $this->getModelName() . '.', $conditions);
        }

        parent::andWhere($conditions, $bindParams, $bindTypes);

        return $this;
    }

    /**
     * Sets class alias.
     *
     * @param string $alias
     * @return $this|CriteriaInterface
     */
    public function alias(string $alias): CriteriaInterface
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get the total number of paginated items.
     *
     * @return int
     */
    public function getPageCount(): int
    {
        return (int)$this->pgCount ?: 0;
    }

    /**
     * Adds the paginate parameter to the criteria.
     *
     * @param int|null $pageIndex
     * @param int|null $pageSize
     * @return CriteriaInterface
     */
    public function paginate(?int $pageIndex = null, ?int $pageSize = null): CriteriaInterface
    {
        $uri = $this->request->getURI();

        // The total number of paginated items.
        $this->pgCount = (clone $this)->columns('count(*) ph_count')->execute()->getFirst()->ph_count;

        if (is_null($pageIndex)) {
            preg_match('/page\/\d+/', $uri, $pageIndex);
            $pageIndex = preg_replace('/[^0-9]/', '', $pageIndex[0]);
        }

        if (is_null($pageSize)) {
            preg_match('/limit\/\d+/', $uri, $pageSize);
            $pageSize = preg_replace('/[^0-9]/', '', $pageSize[0]);
        }

        // Adds the limit parameter to the criteria.
        $this->limit(intval($pageSize), $pageSize * ($pageIndex - 1));

        return $this;
    }
}