<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Interfaces;

/**
 * Interface HousekeepInterface
 *
 * @package Eison\Phalcon\Interfaces
 */
interface HousekeepInterface
{
    /**
     * Return result.
     *
     * @return mixed|null
     */
    public function getReturnedValue();
}