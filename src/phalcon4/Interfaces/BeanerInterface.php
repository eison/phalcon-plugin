<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Interfaces;

use Phalcon\Mvc\ControllerInterface;

/**
 * Interface BeanerInterface
 *
 * @package Eison\Phalcon\Interfaces
 */
interface BeanerInterface
{
    /**
     * Get main logic
     *
     * @return ControllerInterface
     */
    public function getCtl(): ControllerInterface;
}