<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Interfaces;

/**
 * Interface MockupInterface
 *
 * @package Eison\Phalcon\Interfaces
 */
interface CollectionInterface
{
    /**
     * Discard itself
     *
     * @return CollectionInterface
     */
    public function clear(): CollectionInterface;
    
    /**
     * All utility classes must implement this interface.
     *
     * @param $collection
     * @return mixed
     */
    public function init($collection);

    /**
     * Counts all elements in an array, or something in an object.
     *
     * @return int
     */
    public function count(): int;

    /**
     * Increments a stored number.
     *
     * @param string     $key
     * @param int|number $value
     * @return CollectionInterface
     */
    public function increment($key, $value = 1): CollectionInterface;

    /**
     * Decrements a stored number.
     *
     * @param string     $key
     * @param int|number $value
     * @return CollectionInterface
     */
    public function decrement($key, $value = 1): CollectionInterface;

    /**
     * Remove collection property.
     *
     * @param string $key
     * @return mixed
     */
    public function delete($key);

    /**
     * Get the value related to the specified key.
     *
     * @param string $key
     * @param mixed  $default
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * Appends a property-value to this collection.
     *
     * @param mixed $key
     * @param mixed $value
     * @return CollectionInterface
     */
    public function set($key, $value = ''): CollectionInterface;

    /**
     * Checks if the collection has a property or field.
     *
     * @param string $key
     * @return bool
     */
    public function has($key): bool;

    /**
     * Called on chained checks.
     *
     * @param string $name
     * @return mixed
     */
    public function __unset(string $name);

    /**
     * Called on chained checks.
     *
     * @param string $name
     * @return bool
     */
    public function __isset(string $name): bool;

    /**
     * This method will be called when serialize.
     *
     * @return array
     */
    public function __sleep(): array;

    /**
     * Writing data to inaccessible (protected or private) or non-existing properties.
     *
     * @param string $key
     * @param mixed  $value
     * @return mixed
     */
    public function __set($key, $value);

    /**
     * Reading from inaccessible (protected or private) or non-existing properties.
     *
     * @param $name property
     * @return mixed|null
     */
    public function __get($name);
    
    /**
     * Convert a PHP object to an associative array.
     *
     * @return array
     */
    public function toArray(): ?array;
}