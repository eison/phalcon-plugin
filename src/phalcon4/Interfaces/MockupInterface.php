<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Interfaces;

/**
 * Interface MockupInterface
 *
 * @package Eison\Phalcon\Interfaces
 */
interface MockupInterface
{
    /**
     * Validates the data before calling sql
     *
     * @return void
     */
     public function validate();

    /**
     * Creates a Phalcon\Mvc\Model\Query without execute it
     *
     * @param $ignore
     * @return string
     */
    public function queryBuilder($ignore);
}