<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Library;

use Eison\Phalcon\Interfaces\CollectionInterface;

/**
 * Class Collection
 *
 * @package     Eison\Phalcon\Library
 * @description Associative object.
 */
class Collection extends \stdClass implements \Serializable, \ArrayAccess, CollectionInterface
{
    /**
     * Collection.
     *
     * @var array
     * @access private
     */
    protected $collections;

    /**
     * SimpleObject constructor.
     *
     * @param array | stdClass $parameters
     * @return void
     */
    public function __construct($parameters = [])
    {
        $this->init($parameters);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (!property_exists(static::class, $name)) {
            return $this->get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($key, $value): void
    {
        $this->set($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function __sleep(): array
    {
        // Cannot return the name of a private member of the parent class.
        return $this->toArray();
    }

    /**
     * @inheritdoc
     */
    public function __isset(string $name): bool
    {
        return $this->has($name);
    }

    /**
     * @inheritdoc
     */
    public function __unset(string $name): void
    {
        $this->delete($name);
    }

    /**
     * @inheritdoc
     */
    public function serialize(): ?string
    {
        return serialize($this->collections);
    }

    /**
     * @inheritdoc
     */
    public function unserialize($collections): void
    {
        $this->collections = unserialize($collections);
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset): void
    {
        $this->delete($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @inheritdoc
     */
    public function has($key): bool
    {
        return isset($this->collections[$key]);
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value = ''): CollectionInterface
    {
        if (is_array($key) || is_object($key)) {
            $this->collections = array_merge($this->collections, (array)$key);
        } else {
            $this->collections[$key] = $value;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function get($key, $default = null)
    {
        $value = $this->collections[$key] ?? $default;

        if (is_string($value)) {
            return is_serialized($value) ? unserialize($value) : $value;
        } else {
            return $value;
        }
    }

    /**
     * @inheritdoc
     */
    public function delete($key): void
    {
        unset($this->collections[$key]);
    }

    /**
     * @inheritdoc
     */
    public function clear(): CollectionInterface
    {
        $this->collections = [];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function init($collection)
    {
        if ($collection) {
            $this->set($collection);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function decrement($key, $value = 1): CollectionInterface
    {
        if ($this->has($key)) {
            $this->collections[$key] -= $value;
        } else {
            $this->collections[$key] = $value;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function increment($key, $value = 1): CollectionInterface
    {
        if ($this->has($key)) {
            $this->collections[$key] += $value;
        } else {
            $this->collections[$key] = $value;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count((array)$this->toArray());
    }

    /**
     * @inheritdoc
     */
    public function toArray(): ?array
    {
        return $this->collections;
    }
}