<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Eison\Phalcon\Interfaces\BeanerInterface;
use Eison\Phalcon\Traits\GetDepend;
use Phalcon\Mvc\ControllerInterface;

/**
 * Class AbstractBeaner
 *
 * @property Phalcon\Mvc\ControllerInterface $controller
 * @package     Eison\Phalcon\Abstracts
 * @description Abstract logic's beanInterface
 */
abstract class AbstractBeaner implements BeanerInterface
{
    /**
     * ControllerInterface.
     *
     * @class
     */
    protected $controller;

    /**
     * @inheritdoc
     */
    final public function getCtl(): ControllerInterface
    {
        return $this->controller;
    }

    /**
     * BeanInterface constructor.
     *
     * @param ControllerInterface $controller
     */
    public function __construct(ControllerInterface $controller)
    {
        $this->controller = $controller;
    }
}