<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Eison\Phalcon\Interfaces\MockupInterface;
use Eison\Utils\Helper\ArrHelper;
use Phalcon\Mvc\ModelInterface;

/**
 * Class AbstractMockup
 *
 * @property array                       $columMap
 * @package      Eison\Phalcon\Abstracts
 * @description  Abstract Mockup
 */
abstract class AbstractMockup implements MockupInterface
{
    /**
     * Creates a Phalcon\Mvc\Model\Query without execute it
     *
     * @param $ignore
     * @return string
     */
    abstract public function queryBuilder($ignore);

    /**
     * Returns the reverse column map if any.
     *
     * @return array|null
     */
    protected function getColumMap(): ?array
    {
        if (!isset($this->columMap)) {
            $this->columMap = $this->getModel()->getModelsMetaData()->getReverseColumnMap($this->getModel());
        }

        return $this->columMap;
    }

    /**
     * Make rows string.
     *
     * @param array  $bindRows
     * @param string $rowsString
     * @return string
     */
    protected function makeRowsString(array $bindRows, string $rowsString = ''): string
    {
        foreach (scrimp($bindRows) as $value) {
            $rowsString .= '`' . $value . '`,';
        }

        return rtrim($rowsString, ',');
    }

    /**
     * Gets filters.
     *
     * @param array  $rows
     * @param string $function
     * @return array
     */
    protected function getFilter(array $rows, string $function = 'array_values'): array
    {
        $columMaps = ArrHelper::getValue($rows, $this->getColumMap());
        $colums    = ArrHelper::rmValue ($columMaps);

        ksort($colums);

        //--- Performance Optimizations. ---//
        $this->bindColumns = array_keys($colums);

        return call_user_func($function, $colums);
    }

    /**
     * Return a Phalcon\Mvc\Model\Query string.
     *
     * @param bool $ignore use an insert ignore (default: false)
     * @return string
     */
    public function getRawSql(bool $ignore = false): string
    {
        return $this->queryBuilder($ignore);
    }

    /**
     * Returns model interface.
     *
     * @return ModelInterface
     */
    public function getModel(): ?ModelInterface
    {
        return $this->model;
    }

    /**
     * Get execute results.
     *
     * @return bool|Phalcon\Db\Result\Pdo
     */
    public function getExecuteResult()
    {
        return $this->getModel()->mockupResult;
    }
}