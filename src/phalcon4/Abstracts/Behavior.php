<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Eison\Phalcon\PhalconPluginError;
use Eison\Phalcon\Traits\GetDepend;
use Phalcon\Mvc\Model\Behavior as ModelBehavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

/**
 * Class Behavior
 *
 * @package     Eison\Phalcon\Abstracts
 * @description Abstract Model behavior trusteeship
 */
abstract class Behavior extends ModelBehavior implements BehaviorInterface
{
    /**
     * @inheritdoc
     */
    public function notify(string $eventType, ModelInterface $model)
    {
        $hook = new \ReflectionClass($this);

        // All exposed methods will be fired in turn.
        if ($hook->hasMethod($eventType) && $hook->getMethod($eventType)->isPublic()) {
            call_user_func([$this, $eventType], $model);
        }
    }

    /**
     * @inheritdoc
     */
    public function missingMethod(ModelInterface $model, string $method, $arguments = [])
    {
        throw new PhalconPluginError('Invalid parameter.', PhalconPluginError::ER_INVALID_PARAMETER);
    }
}