<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Phalcon\Mvc\ControllerInterface;

/**
 * Class BeanInterface
 *
 * @property Phalcon\Mvc\ControllerInterface $controller
 * @package     Eison\Phalcon\Abstracts
 * @description Abstract logic's beanInterface
 */
abstract class BeanInterface
{
    /**
     * ControllerInterface
     *
     * @class
     */
    protected $controller;

    /**
     * Get main logic
     *
     * @return ControllerInterface
     */
    final protected function getCtl(): ControllerInterface
    {
        return $this->controller;
    }

    /**
     * BeanInterface constructor.
     *
     * @param ControllerInterface $controller
     */
    final public function __construct(ControllerInterface &$controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param string $name
     * @param mixed  $parameter
     * @throws \ReflectionException
     * @return mixed
     */
    public function call(string $name, $parameter)
    {
        $class = $this->getCtl();

        $method = new \ReflectionMethod($class, $name);
        $method->setAccessible(true);

        return $method->invoke($class, ...$parameter);
    }

    /**
     * @param string $name
     * @param mixed  $parameter
     * @throws \ReflectionException
     * @return mixed
     */
    public function __call(string $name, $parameter)
    {
        return $this->call($name, $parameter);
    }
}