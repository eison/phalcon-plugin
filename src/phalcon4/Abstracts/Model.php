<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Eison\Phalcon\Db\BatchUpdateMockup;
use Eison\Phalcon\Db\BulkInsertMockup;
use Eison\Phalcon\Db\DuplicateMockup;
use Eison\Phalcon\SystemError;
use Phalcon\Db\Result\Pdo as PdoResult;
use Phalcon\Di;
use Phalcon\Di\DiInterface;
use Phalcon\Helper\Arr;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\CriteriaInterface;
use Phalcon\Mvc\ModelInterface;

/**
 * Class Model
 *
 * @package     Eison\Phalcon\Abstracts
 * @description Abstract model
 */
abstract class Model extends \Phalcon\Mvc\Model
{
    /**
     * Hold's record.
     *
     * @const int
     */
    const DEL_STORE = 0;

    /**
     * Soft delete's record.
     *
     * @const int
     */
    const DEL_REMOVE = 1;

    /**
     * Normal's state.
     *
     * @const int
     */
    const STATUS_NORMAL = 1;

    /**
     * Blocked's record.
     *
     * @const int
     */
    const STATUS_STOP = 0;

    /**
     * Used to descirbe the status of the sql execute.
     *
     * @var int
     */
    protected $autoTrans = 0;

    /**
     * An object-oriented interface.
     *
     * @class
     */
    protected static $criteria;

    /**
     * Primary column must be setted.
     *
     * @var
     */
    public $mockupResult;

    /**
     * Get object-oriented interface.
     *
     * @return CriteriaInterface
     */
    abstract public static function getCriteria();

    /**
     * Executed on each new object.
     *
     * @param array $parameters
     * @return void
     */
    protected function onConstruct($parameters = [])
    {
        // If you want to perform initialization tasks for every instance created,
        // you can rewrite and implement this abstract method.
    }

    /**
     * Executed Once.
     *
     * Call just once,beforce the constructor called and all model metadata is stored statically in the model class
     * models are created once you get any record(like in this loop)
     */
    public function initialize(): void
    {
        // Construction semantic error!!!!!
        $this->errCode = SystemError::ER_SQL_SEMANTIC;

        // Change specific models to make dynamic updates.
        $this->useDynamicUpdate(true);

        // Sets if the model must keep the original record snapshot in memory.
        $this->keepSnapshots(true);

        // Sets a list of attributes that must be skipped from the generated UPDATE statement.
        $this->skipAttributesOnUpdate(['addTime']);

        // If delete any of the two records the status will be updated instead of delete the record.
        $this->addBehavior(
            new SoftDelete(
                [
                    'field' => 'del',
                    'value' => static::DEL_REMOVE,
                ]
            )
        );
    }

    /**
     * Get excute status of the sql result.
     *
     * @return int
     */
    public function getCode(): int
    {
        return $this->errCode;
    }

    /**
     * Automatically open transactions / or get transactions status.
     *
     * @param int $autoTransAct
     * @return null|int
     */
    public function autoTrans(?int $autoTransAct = null): ?int
    {
        // If parameter is empty null, return transaction status.
        if (!is_null($autoTransAct)) {
            return $this->autoTrans = $autoTransAct;
        }

        return $this->autoTrans;
    }

    /**
     * Inserts a model instance. Returning true on success or false otherwise.
     *
     * @param array|null    $dataColumnMap array to transform keys of data to another
     * @param array|null    $whiteList
     * @param array         $data
     * @param \Closure|null $callBack
     * @return ModelInterface
     */
    public static function insert(array $data, ?array $whiteList = null, ?array $dataColumnMap = null, \Closure $callBack = null): ModelInterface
    {
        $model = new static();

        // AOP callback support!!!
        $callBack && $callBack->call($model);

        // Assigns values to a model from an array and inserts a model instance.
        $model->assign($data, $whiteList, $dataColumnMap);
        $model->create();

        return $model;
    }

    /**
     * Create a criteria for a specific model. Calling by static::query()
     *
     * @param DiInterface|null $container
     * @return CriteriaInterface
     */
    public static function query(DiInterface $container = null): CriteriaInterface
    {
        $criteria = static::getCriteria();
        $container = $container ?: Di::getDefault();

        $criteria->setDI($container);
        $criteria->setModelName(static::class);

        return $criteria;
    }

    /**
     * Insert on duplicate key update statement like ...
     *
     * @param array         $parameters
     * @param \Closure|null $callBack
     * @return ModelInterface|static|null
     */
    public static function modify(array $parameters, \Closure $callBack = null): ModelInterface
    {
        $model = static::findFirst($parameters) ?: new static();

        // AOP callback support!!!
        $callBack && $callBack->call($model);

        // Assigns values to a model from an array.
        $model->assign($parameters['_payload']);

        // Inserts or updates a model instance.
        // Returning true on success or false.
        $model->id ? $model->save() : $model->create();

        return $model;
    }

    /**
     * Gets execute result from mockupInterface.
     *
     * @return PdoResult|null
     */
    public function getMockupResult(): ?PdoResult
    {
        return $this->mockupResult;
    }

    /**
     * Allows bulk inserts.
     *
     * @param array         $parameters
     * @param bool          $ignore
     * @param \Closure|null $callBack
     * @return ModelInterface
     */
    public static function bulkInsert(array $parameters, bool $ignore = false, \Closure $callBack = null): ModelInterface
    {
        // Allows bulk inserts.
        $mockup = new BulkInsertMockup(new static());

        // AOP callback support!!!
        $callBack && $callBack->call($mockup->getModel());

        // Sets the insert rows and values.
        $mockup->setRows(array_keys(Arr::first($parameters)));
        $mockup->setValues($parameters);

        // Insert into the database.
        $model = $mockup->insert($ignore);

        // Return execute result.
        $mockup->getExecuteResult() ? $model->fireEvent('afterSave') : $model->fireEvent('notSaved');

        return $model;
    }

    /**
     * Allows batch updates.
     *
     * @param string|array  $payload
     * @param string|array  $condition
     * @param \Closure|null $callBack
     * @return ModelInterface
     */
    public static function batchUpdate($payload, $condition, \Closure $callBack = null): ModelInterface
    {
        // Allows batch update.
        $mockup = new BatchUpdateMockup(new static());

        // AOP callback support!!!
        $callBack && $callBack->call($mockup->getModel());

        // Bind database's dialect.
        $mockup->setPayload($payload);
        $mockup->setCondition($condition);

        // Execute to modify data.
        $model = $mockup->update();

        // Return execute result.
        $mockup->getExecuteResult() ? $model->fireEvent('afterSave') : $model->fireEvent('notSaved');

        return $model;
    }

    /**
     * INSERT INTO ... ON DUPLICATE KEY UPDATE ...
     *
     * @param array         $parameters
     * @param string|array  $updateColumn
     * @param \Closure|null $callBack
     * @return ModelInterface
     */
    public static function onDuplicate(array $parameters, $updateColumn, \Closure $callBack = null): ModelInterface
    {
        // INSERT INTO ... ON DUPLICATE KEY UPDATE ...
        $mockup = new DuplicateMockup(new static());

        // AOP callback support!!!
        $callBack && $callBack->call($mockup->getModel());

        // Insert into 之后的长度 与 update 的长度是不一致的， updateColumn 代表着更新的字段
        // first kes 的字段及长度不能等视为 updateColumn
        $mockup->setRows(array_keys(Arr::first($parameters)));
        $mockup->setValues($parameters);
        $mockup->setUpdate($updateColumn);

        // Execute to modify data.
        $model = $mockup->duplicate();

        // Return execute result.
        $mockup->getExecuteResult() ? $model->fireEvent('afterSave') : $model->fireEvent('notSaved');

        return $model;
    }
}