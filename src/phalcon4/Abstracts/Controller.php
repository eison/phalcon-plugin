<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Abstracts;

use Eison\Phalcon\Interfaces\HousekeepInterface;
use Eison\Utils\Helper\ArrHelper;
use Phalcon\Mvc\Controller as MvcController;
use Phalcon\Mvc\ControllerInterface;

/**
 * Class Controller
 *
 * @package     Eison\Phalcon\Abstracts
 * @description Abstract controller.
 */
abstract class Controller extends MvcController implements HousekeepInterface
{
    /**
     * Set the default value of not require paramater.
     *
     * @var string
     */
    protected $_content, $allowNull = null;

    /**
     * Maintaining and developing an open params.
     *
     * @param array $whiteList
     * @param array $consequences
     * @return array
     */
    protected function expansion(array $whiteList, array $consequences = []): array
    {
        foreach ($whiteList as $key => $value) {
            $consequences[$key] = $this->request->get($key, $value[0] ?? null, $value[1] ?? null);
        }

        // todo [Modify] Register composer class to DI container.
        $consequences = ArrHelper::rmValue($consequences, $this->allowNull);

        return $consequences;
    }

    /**
     * This method only called once.
     * The initialize() method is only called if the beforeExecuteRoute event is executed with success.
     */
    public function initialize()
    {
        $this->_content['cols'] = [];
    }

    /**
     * Set returned data.
     *
     * @param mixed  $content
     * @param string $key
     * @return ControllerInterface
     */
    public function commit($content, string $key = 'cols'): ControllerInterface
    {
        $this->_content[$key] = $content;

        return $this;
    }

    /**
     * Push elements onto the end of result.
     *
     * @param mixed  $parameters
     * @param string $key
     * @return ControllerInterface
     */
    public function push($parameters, string $key = 'cols'): ControllerInterface
    {
        $this->_content[$key] = isset($this->_content[$key]) ? (array)$this->_content[$key] : array();

        array_push($this->_content[$key], $parameters);

        return $this;
    }

    /**
     * Pop the element off the end of array.
     *
     * @param string $key
     * @return mixed
     */
    public function pop(string $key = 'cols')
    {
        return array_pop($this->_content[$key]);
    }

    /**
     * Gets request params from white list.
     *
     * @param string|array $validateClassOrWhiteList
     * @param array|null   $whiteListOrNull
     * @param bool         $isIndexArr
     * @throws \ReflectionException
     * @return array
     */
    public function getParams($validateClassOrWhiteList, $whiteListOrNull = null, $isIndexArr = false): array
    {
        if (gettype($validateClassOrWhiteList) === 'string') {
            // Dispatch a whitelist.
            $consequence = $this->expansion($whiteListOrNull);

            (new $validateClassOrWhiteList())->validate($consequence);
        } else {
            // Dispatch a whitelist.
            $consequence = $this->expansion($validateClassOrWhiteList);
        }

        // Store the original request parameters.
        $this->store->set('getParams', $consequence);

        // Returns whitelist data.
        return $isIndexArr ? array_values($consequence) : $consequence;
    }

    /**
     * @inheritDoc
     */
    public function getReturnedValue($key = 'cols')
    {
        return $this->_content[$key] ?? null;
    }
}