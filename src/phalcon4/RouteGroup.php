<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

use Eison\Phalcon\Traits\GetDepend;
use Eison\Utils\PhpClass\PhpString;
use Phalcon\Helper\Arr;
use Phalcon\Mvc\Router\Group;
use Phalcon\Mvc\Router\RouteInterface;

/**
 * Class RouteGroup
 *
 * @property string $namespace
 * @package     Eison\Phalcon
 */
class RouteGroup extends Group
{
    /**
     * Getting services more quickly in anywhere.
     *
     * @class
     */
    use GetDepend;

    /**
     * Setting the resource path.
     * Only marshalled routes need to register configuration options.
     *
     * @return void
     */
    private function parseResource(): void
    {
        $annotations = $this->annotations()->get($this)->getClassAnnotations();

        if ($annotations) {
            foreach ($annotations as $annotation) {
                switch ($annotation->getName()) {
                    case 'URI':
                        $arguments = $annotation->getArguments();
                        $this->setPrefix(PhpString::sprintf('/%s', Arr::first($arguments)));
                        break;

                    case 'SD':
                        $arguments = $annotation->getArguments();
                        $this->setNamespace(PhpString::sprintf('App\Controllers\%s\\', Arr::first($arguments)));
                        break;
                }
            }
        }
    }

    /**
     * Sets processed namespace name.
     *
     * @param string $namespace
     * @return void
     */
    private function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
    }

    /**
     * RouteGroup constructor.
     *
     * @param null $paths
     * @return void
     */
    public function __construct($paths = null)
    {
        $this->parseResource();

        parent::__construct($paths);
    }
}