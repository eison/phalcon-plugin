<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

use Eison\Phalcon\Traits\GetDepend;

/**
 * Class AppDI
 *
 * @package     Eison\Phalcon
 * @description Getting all injected service by dynamic registration.
 */
class AppDI
{
    /**
     * Getting services more quickly in anywhere.
     *
     * @class
     */
    use GetDepend;
}