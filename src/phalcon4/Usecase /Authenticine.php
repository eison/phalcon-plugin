<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Usecase ;

use App\Library\Helpers\SignatureHelper as Security;
use Eison\Phalcon\Services\ReidsAdapter as StoreAdapter;
use Eison\Phalcon\Library\Collection;
use Eison\Utils\PhpClass\PhpArray;
use Eison\Utils\PhpClass\PhpString;
use Firebase\JWT\JWT;
use Phalcon\Mvc\ModelInterface;

/**
 * Class Authenticine
 *
 * @property App\Services\ReidsAdapter $adapter
 * @package App\Services
 */
class Authenticine
{
    /**
     * @var array
     */
    private $options = [];

    /**
     * Converts and signs a PHP object or array into a JWT string.
     *
     * @param ModelInterface $Model
     * @return array
     * @todo [Modify] Return Collection when all api has been migrated
     */
    private function make(ModelInterface &$Model): array
    {
        $payload = $Model->toArray();

        $token = JWT::encode($payload, APP_KEY, 'HS256');
        $sessionId = sha1(Security::createUuid());

        $payload += PhpArray::merge($this->options, compact('token', 'sessionId'));

        return $payload;
    }

    /**
     * Buliding stored at key
     *
     * @param string $sessionId
     * @return string
     */
    private function buildKey(string $sessionId = null): string
    {
        $bindUser = sha1($this->getOption('clientIp') . $this->getOption('userAgent'));
        
        return PhpString::sprintf('webtoken:%s', md5($bindUser . $sessionId));
    }

    /**
     * Authenticine constructor.
     *
     * @param ReidsAdapter $adapter
     */
    public function __construct(StoreAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Set the session options.
     *
     * @param string $option option name
     * @param mixed  $value  option value
     */
    public function setOption(string $option, $value): void
    {
        $this->options[$option] = $value;
    }

    /**
     * Get the session options
     *
     * @param string $key
     * @param mixed  $default
     * @return mixed
     */
    public function getOption(string $key, $default = null)
    {
        return $this->options[$key] ?? $default;
    }

    /**
     * Seesion store
     *
     * @param ModelInterface $Model
     * @return array
     */
    public function store(ModelInterface &$Model): array
    {
        $collection = $this->make($Model);

        $key = $this->buildKey($collection['sessionId']);

        // keep one day!!
        $this->adapter->setex($key, 86400, $collection);

        return $collection;
    }

    /**
     * Decodes a JWT string into a PHP object.
     *
     * @param string $sessionId
     * @return Collection|null
     */
    public function fetch(string $sessionId): ?Collection
    {
        $key = $this->buildKey($sessionId);

        // Return array or bool false
        $session = $this->adapter->get($key);

        return $session ? new Collection($session) : null;
    }

    /**
     * Decodes a JWT string into a PHP object.
     *
     * @param string $token JWT string.
     * @return mixed
     */
    public function prase(string $token)
    {
        JWT::$leeway = 60;

        try {
            // Parse data from JWT string
            return JWT::decode($token, APP_KEY, array('HS256'));
        } catch (\Throwable $exception) {
            return false;
        }
    }
}