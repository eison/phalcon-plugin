<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

use Eison\Phalcon\AppDI;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @param mixed ...$paramers
 * @return void
 */
function aSuang(...$paramers): void
{
    if (\function_exists('apcu_clear_cache')) {
        \apcu_clear_cache();
    }

    if (\function_exists('opcache_reset')) {
        \opcache_reset();
    }

    $transaction = AppDI::store()->get('db_transactions');

    if (\is_callable($transaction) && $transaction->has('start')) {
        AppDI::db()->rollback();
    }

    foreach ($paramers as $paramer) {
        VarDumper::dump($paramer);
    }

    exit(-1);
}

if (!function_exists('scrimp')) {
    /**
     * Scrimp and save.
     *
     * @param array $expression
     * @return Generator
     */
    function scrimp(array &$expression): \Generator
    {
        yield from $expression;
    }
}

/**
 * Join array elements with a string.
 *
 * @param array  $paramers
 * @param bool   $bindIs
 * @param string $separator
 * @return string
 */
function equal_link(array $paramers, bool $bindIs = false, string $separator = ' AND '): string
{
    $collection = array();

    if ($bindIs) {
        foreach ($paramers as $key => $value) {
            \array_push($collection, \sprintf('%s=:%s:', $key, $key));
        }
    } else {
        foreach ($paramers as $key => $value) {
            \array_push($collection, \sprintf('%s=\'%s\'', $key, $value));
        }
    }

    return \join($separator, $collection);
}

/**
 * @param mixed $jsonString
 * @return bool
 */
function is_jsonStr($jsonString): bool
{
    return preg_match('/^[\{\[].*[\}\]]$/', $jsonString);
}

/**
 * @param mixed $charlist
 * @return bool
 */
function is_serialized($charlist): bool
{
    if (!\is_string($charlist)) {
        return false;
    }

    if ('N;' == $charlist) {
        return true;
    }

    if (!\preg_match('/^([adCObis]):/', $charlist, $badions)) {
        return false;
    }

    switch ($badions[1]) {
        case 'a' :
        case 'O' :
        case 's' :
        case 'C' : {
            return !!\preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $charlist);
        }
        case 'b' :
        case 'i' :
        case 'd' : {
            return !!\preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $charlist);
        }

    }

    return false;
}