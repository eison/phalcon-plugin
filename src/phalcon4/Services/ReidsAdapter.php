<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Services;

use Phalcon\Helper\Arr;

/**
 * Class ReidsAdapter
 *
 * @package     App\Services
 * @description Adapter unified data format entry
 */
class ReidsAdapter extends \Redis
{
    /**
     * @inheritdoc
     */
    public function __construct(array $parameters)
    {
        // Conneting the cache server.
        $this->connect(
            Arr::get($parameters, 'host'),
            Arr::get($parameters, 'port')
        );

        // Sets server's option
        $this->setOption(self::OPT_PREFIX, Arr::get($parameters, 'prefix'));
        $this->setOption(self::OPT_SERIALIZER, Arr::get($parameters, 'serializer'));
    }
}