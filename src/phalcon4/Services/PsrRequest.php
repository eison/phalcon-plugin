<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Services;

/**
 * Class PsrRequest
 *
 * @package     App\Services
 * @description Implementation of the PSR-7 HTTP messaging interface as defined by PHP-FIG
 */
class PsrRequest extends \Phalcon\Http\Request
{
    /**
     * @inheritdoc
     */
    public function getPut(string $name = null, $filters = null, $defaultValue = null, bool $notAllowEmpty = false, bool $noRecursive = false)
    {
        if (strpos($this->getContentType(), 'json') !== false) {
            $consequence = $this->getJsonRawBody(true);

            if (gettype($consequence) != 'array') {
                $consequence = [];
            }

            $this->_putCache = $consequence;
        }

        return parent::getPut($name, $filters, $defaultValue, $notAllowEmpty, $noRecursive);
    }

    /**
     * @inheritdoc
     */
    public function get(string $name = null, $filters = null, $defaultValue = null, bool $notAllowEmpty = false, bool $noRecursive = false)
    {
        $parameters = null;

        if (strpos($this->getContentType(), 'json') !== false) {
            $parameters = static::getPut($name, $filters, null, $notAllowEmpty, $noRecursive);
        }

        if (is_null($parameters)) {
            $parameters = parent::get($name, $filters, null, $notAllowEmpty, $noRecursive);
        }

        if (is_null($parameters)) {
            $method = $this->getMethod();

            if ('PUT' === $method) {
                $parameters = parent::getPut($name, $filters, $defaultValue, $notAllowEmpty, $noRecursive);
            }
        }

        return ('0' === $parameters || 0 === $parameters || '' === $parameters || $parameters)
            ? $parameters
            : $defaultValue;
    }
}