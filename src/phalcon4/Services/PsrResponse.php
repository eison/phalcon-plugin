<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Services;

/**
 * Class JsonTodoResponse
 *
 * @package     App\Services
 * @description Implementation of the PSR-7 HTTP messaging interface as defined by PHP-FIG
 */
class PsrResponse extends \Phalcon\Http\Response
{
    /**
     * @param int        $code       This is an error status code in the response body
     * @param string     $status     Success means successful | fail means fail
     * @param string     $message    Message
     * @param array|null $collection Response body data
     * @return Response
     */
    private function make(int $code, string $status, string $message, $collection = []): Response
    {
        $data = is_object($collection) ? $collection->toArray() : $collection;
        $consequence = compact('code', 'status', 'message', 'data');

        $this->setJsonContent($consequence);

        return $this;
    }

    /**
     * @param array  $collection Response body
     * @param string $message    Error message
     * @param int    $code       This is an error status code in the response body.
     * @return Response
     */
    public function success(array $collection = [], string $message = 'ok', int $code = 0): Response
    {
        return $this->make($code, 'success', $message, $collection);
    }

    /**
     * @param string $message
     * @param int    $code
     * @return Response
     */
    public function error(string $message = 'Unknown error', int $code = 100001): Response
    {
        return $this->make($code, 'fail', $message);
    }
}