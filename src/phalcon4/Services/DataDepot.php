<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Services;

use Phalcon\Helper\Arr;

/**
 * Class DataDepot
 *
 * @property array|\stdClass $collections
 * @package     Eison\Phalcon\Services
 * @description Different data are stored as different collections in the data depot.
 */
class DataDepot
{
    /**
     * Gets properties
     *
     * @param null|int $filter
     * @return ReflectionProperty[]
     */
    private function getProperties($filter = null): array
    {
        return (new \ReflectionObject($this))->getProperties($filter);
    }

    /**
     * Call an undefined property.
     *
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Call an undefined method.
     *
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        $consequence = Arr::first($arguments) ?: null;

        return $this->get($name, $consequence);
    }

    /**
     * DataDepot constructor.
     *
     * @param array $arguments
     * @return void
     */
    public function __construct(array $arguments = [])
    {
        foreach ($arguments as $property => $value) {
            $this->set($property, $value);
        }
    }

    /**
     * Get the element off the end of collections.
     *
     * @param string $property
     * @param mixed  $default
     * @return mixed
     */
    public function get(string $property, $default = null)
    {
        $value = $this->has($property) ? $this->{$property} : $default;

        if (is_string($value)) {
            return is_serialized($value) ? unserialize($value) : $value;
        } else {
            return $value;
        }
    }

    /**
     * Pop the element off the end of collections.
     *
     * @return mixed
     */
    public function pop()
    {
        $props = $this->getProperties( \ReflectionProperty::IS_PUBLIC );
        $prop  = end($props); // Last element.

        if ($prop) {
            $name = $prop->getName();
            $result = $this->get($name);

            $this->delete($name);

            return $result;
        }

        return null;
    }

    /**
     * Push elements onto the end of collections.
     *
     * @param mixed $defaultValue
     * @return void
     */
    public function push($defaultValue = null): void
    {
        $this->set(md5(microtime()), $defaultValue);
    }

    /**
     * Returns all the names stored.
     *
     * @return array
     */
    public function getNames(): array
    {
        $results = array();
        $props   = $this->getProperties( \ReflectionProperty::IS_PUBLIC );

        foreach ($props as $prop) {
            array_push($results, $prop->getName());
        }

        return $results;
    }

    /**
     * Checks if an element exists in the cache.
     *
     * @param string $property
     * @return bool
     */
    public function has(string $property): bool
    {
        return isset($this->{$property});
    }

    /**
     * Rename key name.
     *
     * @param string $old
     * @param string $news
     * @return void
     */
    public function rename(string $old, string $news): void
    {
        $this->{$news} = $this->{$old};
        $this->delete($old);
    }

    /**
     * Deletes data from the adapter.
     *
     * @param string $property
     * @return void
     */
    public function delete(string $property): void
    {
        if ($this->has($property)) {
            unset($this->{$property});
        }

        // Forces collection of any existing garbage cycles.
        gc_collect_cycles();
    }

    /**
     * Stores data in the adapter.
     *
     * @param mixed $property
     * @param mixed $value
     * @return $this
     */
    public function set($property, $value): self
    {
        $this->{$property} = is_array($value) ? serialize($value) : $value;

        return $this;
    }

    /**
     * Add data and update it, if it exists.
     *
     * @param string $name
     * @param mixed  $data
     * @return $this
     */
    public function append(string $name, $data): self
    {
        $collections = $this->get($name, []);

        if (gettype($collections) === 'object') {
            if (is_array($data)) {
                foreach ($data as $property => $value) {
                    $collections->{$property} = $value;
                }
            } else {
                $collections->{md5(microtime())} = $data;
            }
        } else {
            $collections = array_merge($collections, (array)$data);
        }

        return $this->set($name, $collections);
    }

    /**
     * Flushes/clears the cache.
     *
     * @return $this
     */
    public function clear(): self
    {
        $props = $this->getProperties( \ReflectionProperty::IS_PUBLIC );

        foreach ($props as $prop) {
            $name = $prop->getName();

            if ($this->has($name)) {
                $this->delete($name);
            }
        }

        return $this;
    }

    /**
     * Shear plate. (Only once!!!!!)
     *
     * @param string $name
     * @return mixed|null
     */
    public function crop(string $name)
    {
        $result = $this->get($name);

        $this->delete($name);

        return $result;
    }
}