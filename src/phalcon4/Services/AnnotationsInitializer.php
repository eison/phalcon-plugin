<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Plugin.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Services;

use Eison\Phalcon\Injectable;
use Phalcon\Annotations\Annotation;
use Phalcon\Annotations\Reflection;
use Phalcon\Events\Event;
use Phalcon\Helper\Arr;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\ModelInterface;

/**
 * Class AnnotationsInitializer
 *
 * @package     Eison\Phalcon\Services
 * @description This plugin is called after any model is initialized in the models manager.
 */
class AnnotationsInitializer extends Injectable
{
    /**
     * Returns the annotations found in the class docblock.
     *
     * @param Reflection     $reflector
     * @param ModelsManager  $manager
     * @param ModelInterface $model
     * @return void
     */
    final protected function getClassAnnotations(Reflection $reflector, ModelsManager $manager, ModelInterface $model): void
    {
        $annotations = $reflector->getClassAnnotations();

        if ($annotations) {
            foreach ($annotations as $annotation) {
                $method = '_' . $annotation->getName();

                if (method_exists($this, $method)) {
                    $this->{$method}($annotation, $manager, $model);
                }
            }
        }
    }

    /**
     * Setup a 1-n relation between two models.
     *
     * @param Annotation     $annotation
     * @param ModelsManager  $manager
     * @param ModelInterface $model
     * @return void
     */
    protected function _hasMany(Annotation $annotation, ModelsManager $manager, ModelInterface $model): void
    {
        $arguments = $annotation->getArguments();

        $method = new \ReflectionMethod($model, 'hasMany');
        $method->setAccessible(true);
        $method->invoke($model, ...$arguments);
    }

    /**
     * Sets the mapped source for a model.
     *
     * @param Annotation     $annotation
     * @param ModelsManager  $manager
     * @param ModelInterface $model
     * @return void
     */
    protected function _source(Annotation $annotation, ModelsManager $manager, ModelInterface $model): void
    {
        $arguments = $annotation->getArguments();
        $manager->setModelSource($model, Arr::first($arguments));
    }

    /**
     * This is called after initialize the model, auto trigger.
     *
     * @param Event          $event
     * @param ModelsManager  $manager
     * @param ModelInterface $model
     * @return void
     */
    final public function afterInitialize(Event $event, ModelsManager $manager, ModelInterface $model)
    {
        // Get the annotations defined on a class
        $reflector = $this->annotations->get($model);

        // Parse annotations
        $this->getClassAnnotations($reflector, $manager, $model);
    }
}