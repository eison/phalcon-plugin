<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

/**
 * Class SystemError
 *
 * @错误码说明：
 *   001 调用了一个不存在的(类型、对象、属性、或者方法)[1]，并且[1]没有被魔术法术接管
 *   003 数据库SQL方言构建失败
 *   004 没有操作权限
 *   006 程序运行中函数调用的参数错误
 *   007 用户请求参数格式验证失败，此错误通常发生在逻辑控制器之前
 *   008 数据模型字段格式验证失败，此错误通常发生在CURD之前，逻辑控制器之后
 *   009 数据模型操作错误保存失败，此错误通常发生在CURD之前，逻辑控制器之后
 *   010 请求到数据为空。整个流程未发生任何异常，所以此状态码本质上不能算为异常
 *
 * @package     Eison\Phalcon
 * @description Listen all exception and error.
 */
class SystemError extends \ErrorException implements \Throwable
{
    //*  Default general exception error code  *//
    const ER_SUCCESS           = 0;
    const ER_CALL_UNDFINED     = 110001;
    const ER_TRANSACTION_FAIL  = 110002;
    const ER_SQL_SEMANTIC      = 110003;
    const ER_NOT_PERMITTED     = 110004;
    const ER_INVALID_PARAMETER = 110006;
    const ER_INPUT_VALIDATOR   = 110007;
    const ER_MODEL_VALIDATOR   = 110008;
    const ER_MODEL_NOT_SAVED   = 110009;
    const ER_RESOURCE_NOTFOUND = 110010;

    /**
     * Will be returned data.
     *
     * @var
     */
    protected $payloads;

    /**
     * Overriding exception number code.
     *
     * @param mixed $code
     * @return void
     */
    final protected function setCode($code = ''): void
    {
        $this->payloads['code'] = intval($code);
    }

    /**
     * Overriding exception message.
     *
     * @param string $message
     * @return void
     */
    final protected function setMessage(string $message = ''): void
    {
        $this->payloads['message'] = $message;
    }

    /**
     * Sets request handle status.
     *
     * @param mixed $status
     * @return void
     */
    final protected function setStatus($status): void
    {
        $this->payloads['status'] = $status;
    }

    /**
     * Sets a custom error message to append into payload，
     *
     * @param mixed  $payload
     * @param string $name
     * @return void
     */
    final protected function setPayload(string $name, $payload): void
    {
        $this->payloads[$name] = $payload;
    }

    /**
     * Gets a custom error message to append into payload.
     *
     * @param string $name
     * @return mixed
     */
    final protected function getPayload(string $name)
    {
        return $this->payloads[$name] ?? null;
    }

    /**
     * Gets a custom error message to append into payload.
     *
     * @param string $name
     * @return mixed
     */
    final protected function getPayloads()
    {
        return $this->payloads;
    }

    /**
     * Constructs the exception
     * The format and parameters cannot be changed.
     *
     * @param string $message  The Exception message to throw.
     * @param int    $code     The Exception code.
     * @param int    $severity The severity level of the exception.
     * @param string $filename The filename where the exception is thrown.
     * @param int    $lineno   The line number where the exception is thrown.
     * @param Error  $previous The previous exception used for the exception chaining.
     * @throws \ReflectionException
     * @return void
     */
    public function __construct(
        string $message = '',
        int $code = self::ER_CALL_UNDFINED,
        int $severity = 1,
        string $filename = __FILE__,
        int $lineno = __LINE__,
        Error $previous = null
    )
    {
        $this->setMessage($message);
        $this->setCode($code);

        parent::__construct($message, $code, $severity, $filename, $lineno, $previous);
    }
}