<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon\Middlewares;

use Eison\Phalcon\Injectable;
use Phalcon\Events\Event;
use Phalcon\Http\Request;
use Phalcon\Mvc\Dispatcher;

/**
 * Class AllowOriginMiddleware
 *
 * @package     Eison\Phalcon\Events
 * @description Before entering in the dispatch loop. The Dispatcher only knows the information passed by the Router.
 */
class AllowOriginMiddleware extends Injectable
{
    /**
     * Is preflight request.
     * 
     * @param Request $request
     * @return bool
     */
    protected function isPreflightRequest(Request $request): bool
    {
        return $this->isCorsRequest($request)      // Cors
            && 'OPTIONS' === $request->getMethod() // pre-check request
            && !empty($request->getHeader('Access-Control-Request-Method'));
    }

    /**
     * Gets http scheme.
     * 
     * @param Request $request
     * @return string
     */
    protected function getSchemeAndHttpHost(Request $request): string
    {
        return $request->getScheme() . '://' . $request->getHttpHost();
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isSameHost(Request $request): bool
    {
        return $request->getHeader('Origin') === $this->getSchemeAndHttpHost($request);
    }

    /**
     * Gets request origin addr.
     * 
     * @param Request $request
     * @return string
     */
    protected function getOrigin(Request $request): string
    {
        return $request->getHeader('Origin') ?: '*';
    }

    /**
     * Is CORS request.
     * 
     * @param Request $request
     * @return bool
     */
    protected function isCorsRequest(Request $request): bool
    {
        return !empty($request->getHeader('Origin')) && !$this->isSameHost($request);
    }

    /**
     * Hook.
     *
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $request = $dispatcher->getDI()->getRequest();
        $response = $dispatcher->getDI()->getResponse();

        if ($this->isCorsRequest($request)) {
            $response
                ->setHeader('Access-Control-Allow-Origin' , $this->getOrigin($request))
                ->setHeader('Access-Control-Allow-Headers', 'Content-Type,If-Match,If-Modified-Since,If-None-Match,If-Unmodified-Since,Authorization,sessionid')
                ->setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE')
                ->setHeader('Access-Control-Max-Age'      , '86400')
                ->setHeader('Access-Control-Allow-Credentials', 'true');
        }

        if ($this->isPreflightRequest($request)) {
            $response->setStatusCode(200, 'OK')->send();
            exit;
        }
    }
}