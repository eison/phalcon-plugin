<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

use Eison\Phalcon\Traits\GetDepend;

/**
 * Class Injectable
 *
 * @property \Phalcon\Mvc\Dispatcher                $dispatcher
 * @property \Phalcon\Mvc\Router                    $router
 * @property \Phalcon\Url                           $url
 * @property \Phalcon\Http\Request                  $request
 * @property \Phalcon\Http\Response                 $response
 * @property \Phalcon\Http\Response\Cookies         $cookies
 * @property \Phalcon\Filter                        $filter
 * @property \Phalcon\Flash\Direct                  $flash
 * @property \Phalcon\Flash\Session                 $flashSession
 * @property \Phalcon\Events\Manager                $eventsManager
 * @property \Phalcon\Db\Adapter\AdapterInterface   $db
 * @property \Phalcon\Security                      $security
 * @property \Phalcon\Crypt                         $crypt
 * @property \Phalcon\Tag                           $tag
 * @property \Phalcon\Escaper                       $escaper
 * @property \Phalcon\Annotations\Adapter\Memory    $annotations
 * @property \Phalcon\Mvc\Model\Manager             $modelsManager
 * @property \Phalcon\Mvc\Model\MetaData\Memory     $modelsMetadata
 * @property \Phalcon\Mvc\Model\Transaction\Manager $transactionManager
 * @property \Phalcon\Assets\Manager                $assets
 * @property \Phalcon\Di                            $di
 * @property \Phalcon\Session\Bag                   $persistent
 * @property \Phalcon\Mvc\View                      $view
 * @package     Eison\Phalcon
 * @description This class allows to access services in the services container
 */
class Injectable extends \Phalcon\Di\Injectable
{
    /**
     * Getting services more quickly in anywhere.
     *
     * @class
     */
    use GetDepend;
}