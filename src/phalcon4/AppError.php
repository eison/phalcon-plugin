<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | This file is part of the Phalcon Framework Component.
// +----------------------------------------------------------------------
// | Author eison (c) <eison.icu@gmail.com>
// +----------------------------------------------------------------------

namespace Eison\Phalcon;

/**
 * Class AppError
 *
 * @package     Eison\Phalcon
 * @description App interface
 * @author      eison.icu@gmail.com
 * @date        2021-06-17 17:34:09 via Ubuntu
 */
class AppError extends SystemError
{
    // You can define your own exception code and override the _parsexxx methods.
    // However, the messages format and parameters cannot be changed.
}